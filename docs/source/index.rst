.. Debian Member Portfolio Service documentation master file, created by
   sphinx-quickstart on Tue Jan 20 22:27:21 2009.  You can adapt this file
   completely to your liking, but it should at least contain the root `toctree`
   directive.

Debian Member Portfolio Service
===============================

The Debian Member Portfolio Service is a web application that provides links to
information regarding the activities of a person related to the `Debian Project
<http://www.debian.org/>`_.

The service was originally implemented and is hosted by Jan Dittberner at
http://portfolio.debian.net/.

.. toctree::
   :maxdepth: 2

   devdocs
   sourcecode
   credits

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

