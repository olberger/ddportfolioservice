Credits
=======

The Debian Member Portfolio Service contains contributions from several people.

Code
----

  * Jan Dittberner <jandd at debian dot org>
  * Paul Wise <pabs at debian dot org>
  * Olivier Berger <olivier.berger at telecom-sudparis dot eu>

Translations
------------

  * Jan Dittberner
  * Daniel Manzano (Brazilian Portuguese)
  * Izharul Haq (Indonesian)
  * Stéphane Aulery (French)

If you think your name is missing please tell me (Jan Dittberner) about your
contribution and I'll add you.

