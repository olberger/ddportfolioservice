Development of Debian Member Portfolio Service
==============================================

The Debian Member Portfolio Service is implemented in `Python
<http://www.python.org>`_ using the `Pylons
<http://docs.pylonsproject.org/en/latest/docs/pylons.html>`_ web application
framework.

The following sections describe how to setup a local development environment
for the Debian Member Portfolio Service.

All instructions assume that you work on a Debian system. You should use Python
2.7 for development.

Setup of a local development
----------------------------

To start working on the source code you need to have `git`_ installed::

  sudo aptitude install git

.. _git: http://www.git-scm.com/

The canonical git repository for the Debian Member Portfolio Service is
available at http://debianstuff.dittberner.info/git/ddportfolioservice.git. To
get a clone of the source code you change to a directory of your choice and
invoke git clone::

  cd ~/src
  git clone http://debianstuff.dittberner.info/git/ddportfolioservice.git

You should use `virtualenv`_ to separate the development environment from your
system wide Python installation. You can install virtualenv using::

  sudo aptitude install python-virtualenv

.. _virtualenv: https://pypi.python.org/pypi/virtualenv

When you have :command:`virtualenv` installed you should create a virtual
environment for Debian Member Portfolio Service development and install the
requirements using `pip <https://pypi.python.org/pypi/pip>`_::

  mkdir ~/.virtualenvs
  virtualenv --distribute ~/.virtualenvs/dmportfolio
  . ~/.virtualenvs/dmportfolio/bin/activate
  cd ~/src/ddportfolioservice
  pip install -r squeezereq.pip

.. note::

  The Debian Member Portfolio Service instance at http://portfolio.debian.net/
  is running on a Debian Squeeze server, therefore :file:`squeezereq.pip`
  contains dependency versions matching that Debian release.

The dependency download and installation into the virtual environment takes
some time.

After you have your virtual environment ready you need to setup the project for
development::

  python setup.py develop

Debian Member Portfolio Service needs the JQuery JavaScript library to function
properly. The JQuery library is not included in the git clone and must be
copied into the subdirectory
:file:`ddportfolioservice/public/javascript/jquery`. On Debian systems you can
install the package libjs-jquery and place a symlink to the directory
:file:`/usr/share/javascript` into :file:`ddportfolioservice/public`: ::

  sudo aptitude install libjs-jquery
  ln -s /usr/share/javascript ddportfolioservice/public

Prepare for first startup
~~~~~~~~~~~~~~~~~~~~~~~~~

The Debian Member Portfolio Service uses data from the Debian keyring to get
information regarding PGP keys and names related to email addresses. Before you
can run the service you need to fetch a copy of the keyring and prepare it for
use by the code.

.. note::

  You need rsync and gnupg for these tasks::

    sudo aptitude install rsync gnupg

When you have both installed you can run::

  . ~/.virtualenvs/dmportfolio/bin/activate
  ./synckeyrings.sh
  python ddportfolioservice/model/keyringanalyzer.py

The first synchronizes the keyrings in :file:`$HOME/debian/keyring.debian.org`
with files on the `keyring.debian.org <http://keyring.debian.org>`_ host. And
the second generates a key/value database in
:file:`ddportfolioservice/model/keyringcache` that is used by the code.

Run a development server
~~~~~~~~~~~~~~~~~~~~~~~~

Pylons uses PasteScript to run a development server. You can run a development
server using::

  paster serve --reload development.ini

The output of this command should look like the following::

  Starting subprocess with file monitor
  Starting server in PID 31377.
  serving on http://127.0.0.1:5000

You can now access your development server at the URL that is printed by the command.

If you want to stop the development server press :kbd:`Ctrl + C`.

Common development tasks
------------------------

Add new URL
~~~~~~~~~~~

Debian Member Portfolio Service uses a ini style configuration file
:file:`ddportfolioservice/model/ddportfolio.ini` to configure the generated URL
patterns. The actual URL generation is done in
:py:class:`~ddportfolioservice.controllers.ddportfolio.DdportfolioController`
in the
:py:meth:`~ddportfolioservice.controllers.ddportfolio.DdportfolioController.urllist`
method.

If you want to add a new URL type you have to add a line in
:file:`ddportfolio.ini` and an entry in
:py:class:`~ddportfolioservice.controllers.ddportfolio.DdportfolioController`'s
:py:attr:`~ddportfolioservice.controllers.ddportfolio.DdportfolioController._LABELS`
dictionary. The top level dictionary keys correspond to sections in the ini
file. The dictionary values are dictionaries themselves that contain a special
key ``label`` that defines the label of the section in the output and keys for
each entry to be rendered in that section. The values in these sub-dictionaries
are strings marked for translation using the :py:func:`~pylons.i18n._` function from
:py:mod:`pylons.i18n`.

The patterns in :file:`ddportfolio.ini` can contain the following placeholders
that are filled at runtime:

================== ========================================
Placeholder        Replacement
================== ========================================
%(aliothusername)s user name on `alioth.debian.org`_
%(email)s          email address (URL encoded)
%(emailnoq)s       email address
%(firstchar)s      first character of the email address
%(forumsid)d       forum user id
%(gpgfp)s          GNUPG/PGP key fingerprint
%(name)s           full name (i.e. John Smith)
%(username)s       Debian user name
%(wikihomepage)s   full name in camel case (i.e. JohnSmith)
================== ========================================

.. _alioth.debian.org: http://alioth.debian.org/

The replacement of placeholders is performed in the
:py:meth:`~ddportfolioservice.controllers.ddportfolio.DdportfolioController.urllist`
method. And uses data from the Debian keyring. Access to the pre-parsed keyring
data is performed using the
:py:func:`~ddportfolioservice.model.dddatabuilder.build_data` function of the
module :py:mod:`ddportfolioservice.model.dddatabuilder`, which uses several
helper functions from :py:mod:`ddportfolioservice.model.keyfinder` to access
the key information.

