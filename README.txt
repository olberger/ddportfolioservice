
This is the source code for the Debian Member Portfolio Service
application [0].

Cf. https://debian-member-portfolio-service.readthedocs.org/ for more
documentation (or its source in docs/source/devdocs.rst), including 
how to configure a development environment.


[0] http://wiki.debian.org/DDPortfolio

