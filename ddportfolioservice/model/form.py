# -*- python -*-
# -*- coding: utf-8 -*-
#
# DDPortfolio service form handling model
# Copyright © 2009, 2010, 2011, 2012 Jan Dittberner <jan@dittberner.info>
#
# This file is part of DDPortfolio service.
#
# DDPortfolio service is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# DDPortfolio service is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
import formencode


class DeveloperData(formencode.Schema):
    """Validation schema for DeveloperData."""
    allow_extra_fields = True
    filter_extra_fields = True
    email = formencode.validators.Email(not_empty=True)
    name = formencode.validators.String(not_empty=True)
    gpgfp = formencode.All(formencode.validators.PlainText(),
                           formencode.validators.MinLength(32),
                           formencode.validators.MaxLength(40))
    username = formencode.validators.PlainText()
    nonddemail = formencode.validators.Email()
    aliothusername = formencode.validators.PlainText()
    mode = formencode.validators.OneOf([u'json', u'html'], if_missing=u'html')
    forumsid = formencode.validators.Int(if_missing=None)
    wikihomepage = formencode.validators.String(if_missing=None)


class DDDataRequest(formencode.Schema):
    """Validation schema for DDData request."""
    allow_extra_fields = True
    filter_extra_fields = False
    email = formencode.validators.Email(not_empty=True)
