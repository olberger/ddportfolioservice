# -*- python -*-
# -*- coding: utf8 -*-
#
# DDPortfolio service url builder
# Copyright © 2009, 2010, 2011, 2012 Jan Dittberner <jan@dittberner.info>
#
# This file is part of DDPortfolio service.
#
# DDPortfolio service is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# DDPortfolio service is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
"""
This module provides the function build_urls to build personalized
URLs using the given information and the URL patterns defined in
ddportfolio.ini.
"""

from ConfigParser import ConfigParser, InterpolationMissingOptionError
import pkg_resources
from ddportfolioservice.model import keyfinder
from urllib import quote_plus
from pylons.i18n.translation import _, N_


my_config = ConfigParser()
my_config.readfp(pkg_resources.resource_stream(__name__, 'ddportfolio.ini'))

_FIELDNAMES_MAP = {
    'email': N_('Email address'),
    'name': N_('Name'),
    'gpgfp': N_('GPG fingerprint'),
    'username': N_('Debian user name'),
    'nonddemail': N_('Non Debian email address'),
    'aliothusername': N_('Alioth user name'),
    }


class DDPortfolioEntry(object):
    def __init__(self, config, section, key):
        self.name = key
        self.optional = config.has_option(section, key + '.optional') and \
            config.getboolean(section, key + '.optional') or False
        if config.has_option(section, key + '.type'):
            self.type = config.get(section, key + '.type')
        else:
            self.type = 'url'


def build_urls(fields):
    """Build personalized URLs using the developer information in
    fields."""
    data = []
    qfields = {}
    for key, value in fields.iteritems():
        if value is not None:
            if isinstance(value, unicode):
                qfields[key] = quote_plus(value.encode('utf8'))
            elif isinstance(value, str):
                qfields[key] = quote_plus(value)
            else:
                qfields[key] = value

    if 'gpgfp' not in qfields:
        fpr = keyfinder.getFingerprintByEmail(fields['email'].encode('utf8'))
        if fpr:
            qfields['gpgfp'] = fpr[0]
    qfields['firstchar'] = fields['email'][0].encode('utf8')
    qfields['emailnoq'] = fields['email'].encode('utf8')
    for section in [section.strip() for section in \
                        my_config.get('DEFAULT',
                                      'urlbuilder.sections').split(',')]:
        data.append(['section', section])
        if my_config.has_option(section, 'urls'):
            for entry in ([
                    DDPortfolioEntry(my_config, section, url) for url in \
                        my_config.get(section, 'urls').split(',')]):
                try:
                    data.append(
                        ['url', section, entry,
                             my_config.get(section, entry.name + '.pattern',
                                           False, qfields)])
                except InterpolationMissingOptionError, e:
                    if not entry.optional:
                        if e.reference in _FIELDNAMES_MAP:
                            data.append(['error', section, entry,
                                         _('Missing input: %s') % \
                                             _(_FIELDNAMES_MAP[e.reference])])
                        else:
                            data.append(['error', section, entry,
                                         _('Missing input: %s') % e.reference])
    return data
