# -*- python -*-
# -*- coding: utf-8 -*-
#
# DDPortfolio service key finder module
# Copyright (c) 2009, 2010, 2011, 2012 Jan Dittberner <jan@dittberner.info>
#
# This file is part of DDPortfolio service.
#
# DDPortfolio service is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# DDPortfolio service is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
"""
This module provides tools for finding PGP key information from a
given keyring.
"""
import logging
import time
import sys

db = None
cachetimestamp = 0


def _get_keyring_cache():
    global db, cachetimestamp
    if db is None or (time.time() - cachetimestamp) > 86300:
        import anydbm
        import pkg_resources
        import os.path
        filename = pkg_resources.resource_filename(__name__,
                                                   'keyringcache')
        logging.debug('reading cache data from %s', filename)
        assert os.path.exists(filename) and os.path.isfile(filename)
        db = anydbm.open(filename, 'r')
        cachetimestamp = time.time()
    return db


def _get_cached(cachekey):
    cache = _get_keyring_cache()
    logging.debug('cache lookup for %s', cachekey)
    if cachekey in cache:
        logging.debug('found entry %s', cache[cachekey])
        return cache[cachekey]
    return None


def getFingerprintByEmail(email):
    """
    Gets the fingerprints associated with the given email address if
    available.
    """
    return _get_cached('fpr:email:%s' % email)


def getRealnameByEmail(email):
    """
    Gets the real names associated with the given email address if
    available.
    """
    return _get_cached('name:email:%s' % email)


def getLoginByEmail(email):
    """
    Gets the logins associated with the given email address if
    available.
    """
    return _get_cached('login:email:%s' % email)


def getLoginByFingerprint(fpr):
    """
    Gets the login associated with the given fingerprint if available.
    """
    return _get_cached('login:fpr:%s' % fpr)

def _dump_cache():
    cache = _get_keyring_cache()
    fprs = []
    for key in cache.keys():
        if key.startswith('email:fpr:'):
            fpr = key.replace('email:fpr:', '')
            if not fpr in fprs:
                fprs.append(fpr)
        
    for fpr in fprs:
        login = getLoginByFingerprint(fpr)
        email = _get_cached('email:fpr:%s' % fpr)
        name = _get_cached('name:fpr:%s' % fpr)
        
        print fpr, login, ':'
        print '   ', name, email
        
        
if __name__ == '__main__':
    logging.basicConfig(stream=sys.stderr, level=logging.WARNING)
    _dump_cache()
