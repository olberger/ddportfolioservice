## -*- coding: utf-8 -*-
<%inherit file="base.mako" />\
<%doc>
Template for the url output page.
Copyright © 2009, 2010 Jan Dittberner <jan@dittberner.info>

This file is part of DDPortfolio service.

DDPortfolio service is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

DDPortfolio service is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public
License along with this program.  If not, see
<http://www.gnu.org/licenses/>.
</%doc>\
<%def name="titleaddon()">
 - ${_('Your personal links')}
</%def>
% if c.urldata:
<fieldset id="ddportfolio">
  <legend>${_('Debian Member Portfolio')}</legend>
  <table id="urltable">
    <thead>
      <tr><th>${_('Usage')}</th><th>${_('URL')}</th></tr>
    </thead>
    <tbody>
      % for row in c.urldata:
      % if row[0] == 'section':
      <tr class="section"><th class="sectionlabel" colspan="2">${row[2]}</th></tr>
      <% urlclass = 'odd' %>
      % elif row[0] == 'error':
      <tr class="error">
        <td>${h.literal(h.textile(row[4]))}</td>
        <td>${_('Error during URL creation:')}
          <span class="errormsg">${row[3].replace("\n",
            '<br />')}</span></td>
      </tr>
      % else:
      <tr class="url ${urlclass}">
        <td>${h.literal(h.textile(row[4]))}</td>
        <td>
          % if row[2].type == 'url':
          ${h.link_to(h.truncate(row[3], length=120), row[3])}
          % else:
          <tt>${row[3]}</tt>
          % endif
        </td>
      </tr>
      <%
         if urlclass == 'odd':
            urlclass = 'even'
         else:
            urlclass = 'odd'
      %>
      % endif
      % endfor
    </tbody>
  </table>
</fieldset>
% endif
<p>${h.link_to(_('Restart'), h.url(controller='ddportfolio', action='index'))}</p>
