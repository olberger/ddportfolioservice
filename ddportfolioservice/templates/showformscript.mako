## -*- coding: utf-8 -*- \
<%doc>
Helper JavaScript for the data input form.
Copyright © 2009, 2010 Jan Dittberner <jan@dittberner.info>

This file is part of DDPortfolio service.

DDPortfolio service is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

DDPortfolio service is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public
License along with this program.  If not, see
<http://www.gnu.org/licenses/>.
</%doc>\

var defaulthiddendivs = new Array(
    '#namefield', '#gpgfpfield', '#usernamefield', '#nonddemailfield',
    '#aliothusernamefield', '#wikihomepagefield', '#forumsidfield');
var maskedfielddivs = new Array(
    '#namefield', '#gpgfpfield', '#usernamefield', '#nonddemailfield',
    '#aliothusernamefield', '#wikihomepagefield', '#forumsidfield');
var allfielddivs = new Array(
    '#namefield', '#gpgfpfield', '#usernamefield', '#nonddemailfield',
    '#aliothusernamefield', '#wikihomepagefield', '#forumsidfield');

function updateFields(data, textStatus) {
    if (data.type == 2) { // DD
        $('#name').attr('value', data.name).attr('readonly', 'readonly');
        $('#gpgfp').attr('value', data.gpgfp);
        $('#username').attr('value', data.username).attr(
            'readonly', 'readonly');
        $('#nonddemail').attr('value', data.email).focus();
        $('#aliothusername').attr('value', data.username);
        $('#wikihomepage').attr('value', data.wikihomepage);

        $('#namefield').show();
        $('#gpgfpfield').show();
        $('#usernamefield').show();
        $('#nonddemailfield').show();
        $('#aliothusernamefield').show();
        $('#wikihomepagefield').show();
        $('#forumsidfield').show();

        $('#nonddemail').focus().select();
    } else if (data.type == 1) { // DM
        $('#name').attr('value', data.name).attr('readonly', 'readonly');
        $('#gpgfp').attr('value', data.gpgfp);
        $('#username').attr('value', '');
        $('#nonddemail').attr('value', data.email).focus();
        $('#wikihomepage').attr('value', data.wikihomepage);

        $('#namefield').show();
        $('#gpgfpfield').show();
        $('#usernamefield').hide();
        $('#nonddemailfield').hide();
        $('#aliothusernamefield').show();
        $('#wikihomepagefield').show();
        $('#forumsidfield').show();

        $('#aliothusername').focus().select();
    } else {
        $('#nonddemail').attr('value', data.email);
        $('#name').removeAttr('readonly');
        $('#username').removeAttr('readonly').attr('value', '');
        $('#gpgfp').attr('value', '');

        $('#usernamefield').hide();
        $('#gpgfpfield').hide();
        $('#nonddemailfield').hide();
        $('#namefield').show();
        $('#aliothusernamefield').show();
        $('#wikihomepagefield').show();
        $('#forumsidfield').show();

        $('#name').focus().select();
    }
}

function onChangeShowAll(event) {
    if ($('#showall').attr('checked')) {
        for (var fielddiv in allfielddivs) {
            $(allfielddivs[fielddiv]).show();
        }
    } else {
        for (var fielddiv in maskedfielddivs) {
            $(maskedfielddivs[fielddiv]).hide();
        }
    }
}

function onBlurEmail() {
    if ($.trim($('#email').attr('value')).length > 0) {
        $.ajax({
            'url' : '${h.url(controller="showformscripts", action="fetchdddata")}',
            'data' : {'email' : $('#email').attr('value')},
            'dataType' : 'json',
            'success' : updateFields,
            'error' : function(request, textStatus, errorThrown) {
                $('#email').focus();
            }
        });
    }
}

$(document).ready(function() {
    for (var index in defaulthiddendivs) {
        if (!$(defaulthiddendivs[index]).hasClass('witherrors')) {
            $(defaulthiddendivs[index]).hide();
        }
    }

    $('#showall').attr('checked', false).change(onChangeShowAll);
    $('#showallfield').show();
    $('#email').blur(onBlurEmail).focus();
});
