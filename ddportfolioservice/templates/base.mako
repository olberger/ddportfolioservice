## -*- coding: utf-8 -*- \
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
          "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%doc>
Base template for XHTML templates.
Copyright © 2009, 2010 Jan Dittberner <jan@dittberner.info>

This file is part of DDPortfolio service.

DDPortfolio service is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

DDPortfolio service is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public
License along with this program.  If not, see
<http://www.gnu.org/licenses/>.
</%doc>
<html>
  <head>
    <title>${_('Debian Member Portfolio Service')}${self.titleaddon()}</title>
    ${h.stylesheet_link(h.url('/stylesheets/style.css'))}
    ${self.extrahead()}
  </head>
  <body>
    <div id="header">
      ${h.image(h.url('/images/openlogo-100.jpg'), _('Debian Logo'), 100, 100,
      id='debianlogo')}
      <h1>${_('Debian Member Portfolio Service')}</h1>
      <p>${h.literal(_('''This service has been inspired by Stefano Zacchiroli's <a href="http://wiki.debian.org/DDPortfolio">DDPortfolio page in the Debian Wiki</a>. You can create a set of customized links leading to a Debian Member's or package maintainer's information regarding Debian.'''))}</p>
      <p><a class="FlattrButton" style="display:none" title="Debian Member Portfolio Service" href="${request.scheme}://portfolio.debian.net/">Debian Member Portfolio Service</a></p>
    </div>
    <div id="content">
      ${self.body()}
    </div>
    <div id="footer">
      ${h.image(h.url('/images/agplv3-88x31.png'), _('AGPL - Free Software'), 88, 31,
      id='agpllogo')}
      <p>${h.literal(_('''The service is available under the terms of the <a href="http://www.gnu.org/licenses/agpl.html">GNU Affero General Public License</a> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. You can <a href="%(browseurl)s" title="Gitweb repository browser URL">browse the source code</a> or clone it from <a href="%(cloneurl)s" title="git clone URL">%(cloneurl)s</a> using <a href="http://git-scm.com/">git</a>. If you want to translate this service to your language you can contribute at <a href="%(transifexurl)s" title="Debian Member Portfolio Service at Transifex">Transifex</a>.''') % dict((('browseurl', 'http://debianstuff.dittberner.info/gitweb.cgi?p=ddportfolioservice.git;a=summary'), ('cloneurl', 'http://debianstuff.dittberner.info/git/ddportfolioservice.git'), ('transifexurl', 'https://www.transifex.com/projects/p/debportfolioservice/'))))}</p>
      <p>${_(u'''Copyright © 2009, 2010, 2011, 2012 Jan Dittberner''')}</p>
    </div>
    <script type="text/javascript">
	  var flattr_url = '${request.scheme}://portfolio.debian.net/';
    </script>
    <script src="${request.scheme}://api.flattr.com/js/0.6/load.js?mode=auto&amp;button=compact" type="text/javascript"></script>
  </body>
</html>
<%def name="extrahead()"></%def>
