## -- coding: utf-8 -- \
<%inherit file="base.mako" />
<%doc>
Template for the data input form.
Copyright © 2009, 2010 Jan Dittberner <jan@dittberner.info>

This file is part of DDPortfolio service.

DDPortfolio service is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

DDPortfolio service is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public
License along with this program.  If not, see
<http://www.gnu.org/licenses/>.
</%doc>
<%def name="titleaddon()">
 - ${_('Enter your personal information')}
</%def>
<%def name="extrahead()">${h.javascript_link('/javascript/jquery/jquery.js',
h.url(controller='showformscripts', action='index'))}</%def>
${h.form(h.url(action='urllist', controller='ddportfolio'), method='get')}
<fieldset id="ddportfolio">
  <legend>${_('Debian Member Portfolio')}</legend>
  <div id="emailfield" \
       % if 'email' in c.messages['errors']:
       class="witherrors" \
       % endif
       >
    <label for="email">${_('Email address:')}
      % if 'email' in c.messages['errors']:
      <br />
      <span class="errormsg">${c.messages['errors']['email'] | h}</span>
      % endif
    </label><br />
    ${h.text('email',
             h.escape(request.params.get('email', None), True), id='email')}<br />
  </div>
  <div id="showallfield" class="hidden">
    ${h.checkbox('showall', value='1', checked=False, id='showall')}
    <label for="showall">${_(u'Show all form fields')}</label><br />
  </div>
  <div id="namefield" \
       % if 'name' in c.messages['errors']:
       class="witherrors" \
       % endif
       >
    <label for="name">${_('Name:')}
      % if 'name' in c.messages['errors']:
      <br />
      <span class="errormsg">${c.messages['errors']['name'] | h}</span>
      % endif
    </label><br />
    ${h.text('name',
             h.escape(request.params.get('name', None)), id='name')}<br />
  </div>
  <div id="gpgfpfield">
    <label for="gpgfp">${_('GPG fingerprint:')}
      % if 'gpgfp' in c.messages['errors']:
      <br />
      <span class="errormsg">${c.messages['errors']['gpgfp'] | h}</span>
      % endif
    </label><br />
    ${h.text('gpgfp',
             h.escape(request.params.get('gpgfp', None)),
             id='gpgfp')}<br />
  </div>
  <div id="usernamefield" \
       % if 'username' in c.messages['errors']:
       class="witherrors" \
       % endif
       >    
    <label for="username">${_('Debian user name:')}
      % if 'username' in c.messages['errors']:
      <br />
      <span class="errormsg">${c.messages['errors']['username'] | h}</span>
      % endif
    </label><br />
    ${h.text('username',
             h.escape(request.params.get('username', None)),
             id='username')}<br />
  </div>
  <div id="nonddemailfield" \
       % if 'nonddemail' in c.messages['errors']:
       class="witherrors" \
       % endif
       >
    <label for="nonddemail">${_('Non Debian email address:') | h}
      % if 'nonddemail' in c.messages['errors']:
      <br />
      <span class="errormsg">${c.messages['errors']['nonddemail'] | h}</span>
      % endif
    </label><br />
    ${h.text('nonddemail',
             h.escape(request.params.get('nonddemail', None)),
             id='nonddemail')}<br />
  </div>
  <div id="aliothusernamefield" \
       % if 'aliothusername' in c.messages['errors']:
       class="witherrors"
       % endif
       >
    <label for="aliothusername">${_('Alioth user name:')}
      % if 'aliothusername' in c.messages['errors']:
      <br />
      <span
         class="errormsg">${c.messages['errors']['aliothusername'] | h}</span>
      % endif
    </label><br />
    ${h.text('aliothusername',
             h.escape(request.params.get('username', None)),
             id='aliothusername')}<br />
  </div>
  <div id="wikihomepagefield" \
       % if 'wikihomepage' in c.messages['errors']:
       class="witherrors"
       % endif
       >
    <label for="wikihomepage">${_('Wiki user name:')}
      % if 'wikihomepage' in c.messages['errors']:
      <br />
      <span class="errormsg">${c.messages['errors']['wikihomepage'] | h}</span>
      % endif
    </label><br />
    ${h.text('wikihomepage',
             h.escape(request.params.get('wikihomepage', None)),
             id='wikihomepage')}<br />
  </div>
  <div id="forumsidfield" \
       % if 'forumsid' in c.messages['errors']:
       class="witherrors"
       % endif
       >
    <label for="forumsid">${_('Forum user id:')}
      % if 'forumsid' in c.messages['errors']:
      <br />
      <span class="errormsg">${c.messages['errors']['forumsid'] | h}</span>
      % endif
    </label><br />
    ${h.text('forumsid',
             h.escape(request.params.get('forumsid', None)),
             id='forumsid')}<br />
  </div>
  <div id="modefield">
    <label for="mode_html">${_('Output format:')}
      % if 'mode' in c.messages['errors']:
      <br />
      <span class="errormsg">${c.messages['errors']['mode'] | h}</span>
      % endif
    </label><br />
    ${_('HTML')}&#160;${h.radio('mode', 'html',
    checked=(request.params.get('mode',
    'html') == 'html'))}&#160;${_('JSON')}&#160;${h.radio('mode',
    'json', checked=(request.params.get('mode', 'html') == 'json'))}<br />
    ${h.submit('submit', value=_('Build Debian Member Portfolio URLs'))}
  </div>
</fieldset>
${h.end_form()}
