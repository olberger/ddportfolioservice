# -*- python -*-
# -*- coding: utf-8 -*-
#
# DDPortfolio service ShowformscriptController.
# Copyright © 2009, 2010, 2011, 2012 Jan Dittberner <jan@dittberner.info>
#
# This file is part of DDPortfolio service.
#
# DDPortfolio service is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# DDPortfolio service is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
import logging
import simplejson

from pylons import request, response
from pylons.controllers.util import abort
import formencode.api
import formencode.validators

from ddportfolioservice.lib.base import BaseController, render
from ddportfolioservice.model.form import DDDataRequest
from ddportfolioservice.model import dddatabuilder

log = logging.getLogger(__name__)


class ShowformscriptsController(BaseController):
    """This controller is used to support data entry in showform.

    It provides code for generating JavaScript as well as JSON
    responses for autocompletion of fields."""

    def index(self):
        """
        This action generates the helper script for the showform page.
        """
        response.headers['Content-Type'] = 'text/javascript; charset=utf-8'
        return render('/showformscript.mako')

    def fetchdddata(self):
        """
        This action fetches the data for a given mail address and
        returns them as JSON.
        """
        schema = DDDataRequest()
        try:
            formencode.api.set_stdtranslation(
                domain="FormEncode",
                languages=[lang[0:2] for lang in request.languages])
            form_result = schema.to_python(request.params)
        except formencode.validators.Invalid, error:
            errors = error.unpack_errors()
            abort(400, "\n".join(
                    ["%s: %s" % (key, errors[key]) for key in errors]))
        fields = dddatabuilder.build_data(form_result['email'])
        log.debug(fields)
        response.headers['Content-Type'] = 'text/plain'
        return simplejson.dumps(fields)
