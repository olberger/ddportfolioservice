# -*- python -*-
# -*- coding: utf-8 -*-
#
# DDPortfolio service DdportfolioController
# Copyright © 2009, 2010, 2011, 2012 Jan Dittberner <jan@dittberner.info>
#
# This file is part of DDPortfolio service.
#
# DDPortfolio service is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# DDPortfolio service is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
import logging
import simplejson

from pylons import request, response, tmpl_context as c
from pylons.i18n import N_, _
import formencode.api
import formencode.validators

from ddportfolioservice.lib.base import BaseController, render
from ddportfolioservice.model.form import DDDataRequest, DeveloperData
from ddportfolioservice.model.urlbuilder import build_urls
from ddportfolioservice.model import dddatabuilder

log = logging.getLogger(__name__)


class DdportfolioController(BaseController):
    """
    Main controller for the Debian Member portfolio service.
    """
    _LABELS = {
        'overview': {
            'label': N_('Overview'),
            'ddpo': N_("Debian Member's Package Overview"),
            'alladdresses': N_("""Debian Member's Package Overview
... showing all email addresses"""),
            },
        'bugs': {
            'label': N_('Bugs'),
            'received': N_('''bugs received
(note: co-maintainers not listed, see \
<a href="http://bugs.debian.org/cgi-bin/bugreport.cgi?\
bug=430986">#430986</a>)'''),
            'reported': N_('bugs reported'),
            'usertags': N_('user tags'),
            'searchall': N_('all messages (i.e., full text search for \
developer name on all bug logs)'),
            'wnpp': N_('<a href="http://wiki.debian.org/WNPP">WNPP</a>'),
            'correspondent': N_('correspondent for bugs'),
            'graph': N_('one year open bug history graph'),
            },
        'build': {
            'label': N_('Build'),
            'buildd': N_('buildd.d.o'),
            'igloo': N_('igloo'),
            },
        'qa': {
            'label': N_('Quality Assurance'),
            'dmd': N_('maintainer dashboard'),
            'lintian': N_('lintian reports'),
            'lintianfull': N_('full lintian reports (i.e. including \
"info"-level messages)'),
            'piuparts': N_('piuparts'),
            'patchtracker': N_('Debian patch tracking system'),
            'duck': N_('Debian Url ChecKer'),
            },
        'lists': {
            'label': N_('Mailing Lists'),
            'dolists': N_('lists.d.o'),
            'adolists': N_('lists.a.d.o'),
            'gmane': N_('gmane'),
            },
        'files': {
            'label': N_('Files'),
            'people': N_('people.d.o'),
            'oldpeople': N_('oldpeople'),
            'alioth': N_('Alioth'),
            },
        'membership': {
            'label': N_('Membership'),
            'nm': N_('NM'),
            'dbfinger': N_('DB information via finger'),
            'db': N_('DB information via HTTP'),
            'webid': N_('FOAF profile'),
            'alioth': N_('Alioth'),
            'wiki': N_('Wiki'),
            'forum': N_('Forum'),
            },
        'miscellaneous': {
            'label': N_('Miscellaneous'),
            'debtags': N_('debtags'),
            'planetname': N_('Planet Debian (name)'),
            'planetuser': N_('Planet Debian (username)'),
            'links': N_('links'),
            'website': N_('Debian website'),
            'search': N_('Debian search'),
            'gpgfinger': N_('GPG public key via finger'),
            'gpgweb': N_('GPG public key via HTTP'),
            'nm': N_('NM, AM participation'),
            'contrib': N_('Contribution information').
            },
        'ssh': {
            'label': N_('Information reachable via ssh (for Debian Members)'),
            'owndndoms': N_('owned debian.net domains'),
            'miainfo': N_('<a href="http://wiki.debian.org/qa.debian.org/'
                          'MIATeam">MIA</a> database information'),
            'groupinfo': N_('Group membership information'),
            },
        'ubuntu': {
            'label': N_('Ubuntu'),
            'ubuntudiff': N_('Available patches from Ubuntu'),
            },
        }

    def _get_label(self, section, url=None):
        if section in self._LABELS:
            if url:
                if url in self._LABELS[section]:
                    return self._LABELS[section][url]
            elif 'label' in self._LABELS[section]:
                return self._LABELS[section]['label']
        if url:
            return "%s.%s" % (section, url)
        return section

    def index(self):
        """
        Render the input form.
        """
        return render('/showform.mako')

    def urllist(self):
        """Handle the actual data."""
        schema = DDDataRequest()
        try:
            formencode.api.set_stdtranslation(
                domain="FormEncode",
                languages=[lang[0:2] for lang in request.languages])
            form_result = schema.to_python(request.params)
        except formencode.validators.Invalid, error:
            c.messages = {'errors': error.unpack_errors()}
            return render('/showform.mako')
        fields = dddatabuilder.build_data(form_result['email'])
        rp = request.params.copy()

        DM_TUPLES = (('name', 'name'),
                     ('gpgfp', 'gpgfp'),
                     ('nonddemail', 'email'))
        DD_TUPLES = (('username', 'username'),
                     ('aliothusername', 'username'))

        if fields['type'] in (dddatabuilder.TYPE_DD, dddatabuilder.TYPE_DM):
            for tuple in DM_TUPLES:
                if not tuple[0] in rp or not rp[tuple[0]]:
                    rp[tuple[0]] = fields[tuple[1]]
        if fields['type'] == dddatabuilder.TYPE_DD:
            for tuple in DD_TUPLES:
                if not tuple[0] in rp or not rp[tuple[0]]:
                    rp[tuple[0]] = fields[tuple[1]]

        schema = DeveloperData()
        try:
            formencode.api.set_stdtranslation(
                domain="FormEncode",
                languages=[lang[0:2] for lang in request.languages])
            form_result = schema.to_python(rp)
        except formencode.validators.Invalid, error:
            c.messages = {'errors': error.unpack_errors()}
            return render('/showform.mako')
        if form_result['wikihomepage'] is None:
            log.debug('generate wikihomepage from name')
            form_result['wikihomepage'] = "".join(
                [part.capitalize() for part in form_result['name'].split()])
        data = build_urls(form_result)
        if form_result['mode'] == 'json':
            response.headers['Content-Type'] = 'text/javascript'
            return simplejson.dumps(
                dict([("%s.%s" % \
                           (entry[1], entry[2].name), entry[3]) \
                          for entry in data if entry[0] == 'url']))
        for entry in data:
            if entry[0] in ('url', 'error'):
                entry.append(_(self._get_label(entry[1], entry[2].name)))
            elif entry[0] == 'section':
                entry.append(_(self._get_label(entry[1])))
        c.urldata = data
        return render('/showurls.mako')
