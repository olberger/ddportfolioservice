# -*- python -*-
# -*- coding: utf-8 -*-
#
# DDPortfolio service base controller
# Copyright © 2009, 2010, 2011, 2012 Jan Dittberner <jan@dittberner.info>
#
# This file is part of DDPortfolio service.
#
# DDPortfolio service is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# DDPortfolio service is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
"""The base Controller API

Provides the BaseController class for subclassing.
"""
from pylons import tmpl_context as c, request
from pylons.controllers import WSGIController
from pylons.i18n import add_fallback
from pylons.templating import render_mako as render


class BaseController(WSGIController):

    def __call__(self, environ, start_response):
        """Invoke the Controller"""
        # WSGIController.__call__ dispatches to the Controller method
        # the request is routed to. This routing information is
        # available in environ['pylons.routes_dict']
        # set language environment
        for lang in request.languages:
            try:
                add_fallback(lang.replace('-', '_'))
            except:
                pass
        c.messages = {'errors': [], 'messages': []}
        return WSGIController.__call__(self, environ, start_response)
