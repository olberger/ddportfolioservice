# -*- python -*-
# -*- coding: utf-8 -*-
#
# DDPortfolio service websetup
# Copyright (c) 2009, 2010, 2011, 2012 Jan Dittberner <jan@dittberner.info>
#
# This file is part of DDPortfolio service.
#
# DDPortfolio service is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# DDPortfolio service is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
"""Setup the ddportfolioservice application"""
import logging

from paste.deploy import appconfig
from pylons import config
import pylons.test

from ddportfolioservice.config.environment import load_environment

log = logging.getLogger(__name__)


def setup_config(command, filename, section, vars):
    """Place any commands to setup ddportfolioservice here"""
    conf = appconfig('config:' + filename)

    if not pylons.test.pylonsapp:
        load_environment(conf.global_conf, conf.local_conf)
