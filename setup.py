# -*- python -*-
# -*- coding: utf-8 -*-
#
# DDPortfolio service setup
# Copyright © 2009, 2010, 2011, 2012, 2013 Jan Dittberner <jan@dittberner.info>
#
# This file is part of DDPortfolio service.
#
# DDPortfolio service is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# DDPortfolio service is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
try:
    from setuptools import setup, find_packages
except ImportError:
    from ez_setup import use_setuptools
    use_setuptools()
    from setuptools import setup, find_packages

setup(
    name='ddportfolioservice',
    version='0.2.19',
    description='service to create DDPortfolio URLs',
    long_description="""This is a service implementation that
returns a set of personalized URLs as outlined in
http://wiki.debian.org/DDPortfolio. It takes the Debian developers
full name and email address as input and returns a JSON formatted
array of URLs.""",
    # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=['Development Status :: 3 - Alpha',
                 'Environment :: Web Environment',
                 'License :: DFSG approved',
                 'License :: OSI approved :: ' +
                 'GNU Affero General Public License v3',
                 'Programming Language :: Python'],
    keywords='Debian service JSON',
    author='Jan Dittberner',
    author_email='jan@dittberner.info',
    url='http://debian-stuff.dittberner.info/ddportfolioservice',
    license='AGPL-3.0+',
    install_requires=["Pylons>=0.10rc1", 'babel>=0.9.4'],
    packages=find_packages(exclude=['ez_setup']),
    include_package_data=True,
    test_suite='nose.collector',
    package_data={'ddportfolioservice': ['*.ini', 'i18n/*/LC_MESSAGES/*.mo']},
    message_extractors = {'ddportfolioservice': [
            ('**.py', 'python', None),
            ('templates/**.mako', 'mako', None),
            ('public/**', 'ignore', None)]},
    zip_safe=False,
    entry_points="""
    [paste.app_factory]
    main = ddportfolioservice.config.middleware:make_app

    [paste.app_install]
    main = pylons.util:PylonsInstaller
    """,
)
