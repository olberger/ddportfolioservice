#!/bin/bash

# Copyright (C) 2009 Jan Dittberner <jandd@debian.org> and others This
# little script is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; version 3.
#
# On Debian systems, the complete text of the GNU General Public
# License can be found in /usr/share/common-licenses/GPL-3 file.
#
# This script syncs local copies of the debian-keyring with
# keyring.d.o (the keyring package is too old).
#
# ~/debian/keyring.debian.org/keyrings/ will be created if it doesn't exist.

# Always exit if there is an error
set -e

# For the rsync of the debian keyrings and for the nm.gpg
DESTDIR="${DEBHOME:-"$HOME/debian"}/keyring.debian.org/keyrings"
# For the following calls use LANG=C - some output is used for
# reports to a list / for an english report.
LANG=C
export LANG

test -d $DESTDIR || mkdir -p $DESTDIR

rsync -qcltz --block-size=8192 --partial --progress --exclude='emeritus-*' --exclude='removed-*' 'keyring.debian.org::keyrings/keyrings/*' $DESTDIR/.
